% Intended LaTeX compiler: pdflatex
\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{sbc-template}
\usepackage[portuguese, ]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage[caption=false]{subfig}
\usepackage{booktabs}
%\usepackage{hyphenat}
\hyphenation{e-la-bo-ra-ção re-pre-sen-tar bi-bli-o-te-ca}
\renewcommand{\figurename}{Figura}
\date{}
\title{Paper for ERAD 2020}
\hypersetup{
 pdfauthor={Marcelo Miletto et al.},
 pdftitle={Paper for ERAD 2020},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.3 (Org mode 9.1.9)}, 
 pdflang={Pt-Br}}
\begin{document}

%\usepackage[brazil]{babel}   

%\title{Análise de Desempenho de Uma Aplicação Baseada em Tarefas Irregulares}
\title{Mecanismo de Detecção de Tarefas Anômalas Para a Análise de Desempenho de Aplicações com Tarefas Irregulares}

\author{
   Marcelo Cogo Miletto\and%\inst{1},
   Lucas Mello Schnorr%\inst{1},
}

\address{
   Programa de Pós-Graduação em Computação (PPGC/UFRGS), Porto Alegre, Brasil
   \email{\{marcelo.miletto, schnorr\}@inf.ufrgs.br}
}

\maketitle

\begin{resumo}
Este trabalho propõe uma metodologia para enriquecer a análise de
desempenho de aplicações baseadas em tarefas, principalmente em
cenários onde o custo das tarefas computacionais não é
uniforme. Consideramos o emprego de regressão estatística para construir um
mecanismo que detecta tarefas anômalas tomando por base o custo estimado por um modelo de desempenho.
\end{resumo}

\noindent
\textbf{Contextualização e Motivação}: Aplicações computacionais provenientes de diversas áreas de pesquisa
necessitam de cada vez mais poder computacional. Essa demanda é
atualmente suportada pela constante evolução de componentes de
hardware voltados para o processamento paralelo e de alto
desempenho. No entanto, o desafio de desenvolver aplicações que
utilizem tais recursos de forma eficiente torna-se cada vez maior. Uma abordagem bastante popular ultimamente é a programação
baseada em tarefas. Tal paradigma permite representar
uma aplicação como de um grafo acíclico dirigido, onde os nós são
as tarefas computacionais e as arestas suas dependências. Esta ideia,
embora seja simples, é bastante poderosa. Ela oferece uma maneira
portável de se obter desempenho de aplicações complexas em
arquiteturas também complexas \cite{dongarra2017extreme}.
%# introdução/contexto
Este desempenho é obtido pelo do emprego de um sistema de
\emph{runtime}. Nele se encontra o escalonador, que tem um papel central em
tais aplicações pois é responsável pela distribuição das
tarefas entre os recursos computacionais. Para fazer isto de forma
eficiente, o escalonador deve considerar aspectos como custos de
comunicação e reutilização de cache, enquanto distribui a carga
uniformemente entre as unidades computacionais. Diversos trabalhos
mostram como esse nível de runtime pode afetar o desempenho de uma
mesma aplicação \cite{miletto2019openmp}. Isto é causado por
sobrecargas no sistema de \emph{runtime} ou por decisões ruins tomadas pelo
escalonador. Uma das formas de destacar possíveis problemas de
desempenho tanto a nível de \emph{runtime} quanto de aplicação, é pela da
análise de tarefas anômalas, que apresentam uma duração maior do que a
esperada. 

\noindent
\textbf{Proposta de Trabalho e Metodologia}: Este trabalho propõe uma
metodologia para a identificação automática de tarefas anômalas em
casos onde a aplicação gera tarefas não homogêneas, como por exemplo,
em computações envolvendo matrizes esparsas. O objetivo deste trabalho
compreende a elaboração deste mecanismo de detecção, e sua integração
em uma ferramenta voltada para a visualização e análise de desempenho
de aplicações baseadas em tarefas, chamada StarVZ
\cite{garcia2018visual}. Essa integração é feita como uma forma de
validação de nossa metodologia proposta, ao mesmo tempo em que permite
explorar casos interessantes para aplicações baseadas em StarPU
\cite{augonnet2011starpu}. O mecanismo de detecção é baseado em um
modelo de regressão, nele, considera-se o número teórico de operações
de ponto flutuante que uma tarefa realiza para estimar sua duração. O
modelo utilizado define \(y_{i}\) como sendo explicado pela variável
\(x_{i}^{2/3}\), onde \(y_{i}\) é o tempo de duração esperado para a tarefa \(i\),
que possui um peso computacional \(x_{i}\). Um modelo é aplicado para cada
tipo de recurso computacional.    
% # metodologia/resultados
A metodologia proposta foi testada em uma aplicação baseada em
tarefas que realiza a fatoração QR de matrizes esparsas
\cite{buttari2013fine}, usando a biblioteca StarPU. Esta aplicação
divide a fatoração em subproblemas, que são formados por submatrizes
menores e mais densas. Cada submatriz é particionada em blocos, sobre
os quais se executa um conjunto de tarefas. Um mesmo tipo de tarefa,
então, pode ter um custo computacional diferente, já que a esparsidade 
dos blocos onde é realizada a computação varia, determinando o número
de operações de ponto flutuante necessárias.   

\noindent
\textbf{Resultados e Conclusão}: 
Para representação destas tarefas na ferramenta StarVZ, um painel de
visualização que gera um gráfico de Gantt foi incrementado com a
informação das tarefas anômalas. Estas tarefas são representadas 
por cores mais fortes, de forma a destacá-las. Com isto, é possível
perceber se a ocorrência de anomalias está associada a algum recurso
computacional ou momento específico. Isso é ilustrado pela Figura
\ref{fig:outlier}, onde a altura no eixo Y representa o núcleo de CPU
em que a tarefa foi executada, e as cores o tipo de tarefa para um
intervalo de tempo da aplicação. Podemos ver em alguns casos, que a
linha que representa as dependências de uma tarefa anômala, aponta
para tarefas executadas em outros núcleos de CPU, o que pode aumentar o
número de cache \emph{misses}. Esta informação sobre as tarefas, pode ajudar
tanto desenvolvedores de aplicações, quanto quem desenvolve
bibliotecas que suportam tal paradigma a investigar e detectar
problemas de desempenho. O material usado neste trabalho está
publicamente disponível em \url{https://gitlab.com/mmiletto/erad\_2020}. 

\begin{figure*}[!htbp]
\centering

  \includegraphics[width=\textwidth]{./img/gantt.pdf}
  \caption{Tarefas anômalas e suas dependências destacadas em um gráfico da ferramenta StarVZ.}
  \label{fig:outlier}
\end{figure*}

\bibliographystyle{sbc}
\bibliography{refs}
\end{document}