LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
LD_LIBRARY_PATH=/home/users/mmiletto/install/spack/opt/spack/linux-ubuntu18.04-haswell/gcc-7.4.0/starpu-1.3.3-2pyv7lz5jjmrphpzxxripicd3ff3xrtt/lib:/home/users/mmiletto/install/spack/opt/spack/linux-ubuntu18.04-haswell/gcc-7.4.0/poti-master-hrbowjdpibo5ccqxs2etmducewzdx233/lib:/home/users/mmiletto/install/pajeng-git/lib:/home/users/mmiletto/install/qr_node_flops_exp_ubuntu_1.3.3/lib:/home/users/mmiletto/install/qr_node_flops_exp_ubuntu_1.3.3/lib64
LC_MEASUREMENT=pt_BR.UTF-8
SSH_CONNECTION=192.168.30.2 51478 192.168.30.18 22
LESSCLOSE=/usr/bin/lesspipe %s %s
LC_PAPER=pt_BR.UTF-8
LC_MONETARY=pt_BR.UTF-8
LANG=en_US.UTF-8
OLDPWD=/scratch/mmiletto/exp_bind_hype5_1_flower_8_4_5_320_-1_32_lws_20_qr_node_flops_exp
STARPU_SCHED=lws
STARPU_NCUDA=0
LC_NAME=pt_BR.UTF-8
XDG_SESSION_ID=660
USER=mmiletto
PWD=/home/users/mmiletto/install/qr_node_flops_exp_ubuntu_1.3.3
HOME=/home/users/mmiletto
LC_CTYPE=pt_BR.UTF-8
SSH_CLIENT=192.168.30.2 51478 22
STARPU_NWORKER_PER_CUDA=0
XDG_DATA_DIRS=/usr/local/share:/usr/share:/var/lib/snapd/desktop
STARPU_FXT_TRACE=1
LC_ADDRESS=pt_BR.UTF-8
STARPU_PROFILING=1
LC_NUMERIC=pt_BR.UTF-8
SCRATCH=/scratch/mmiletto/
INTEL_LICENSE_FILE=/home/intel/licenses/l_3NMTN5T6.lic
SSH_TTY=/dev/pts/0
MAIL=/var/mail/mmiletto
SPACK_ROOT=/home/users/mmiletto/install/spack
SHELL=/bin/bash
TERM=xterm-256color
CONF_FILE=/home/users/mmiletto/install/qr_node_flops_exp_ubuntu_1.3.3/conf.txt
STARPU_PROF_PAPI_EVENTS=PAPI_L1_TCM PAPI_L2_TCM PAPI_L3_TCM
SHLVL=2
LANGUAGE=en_US:en
LC_TELEPHONE=pt_BR.UTF-8
SPACK_LD_LIBRARY_PATH=/home/users/mmiletto/install/spack/opt/spack/linux-ubuntu18.04-haswell/gcc-7.4.0/starpu-develop-mwcgmkfolhjchgurkymy4n2fakrlxpej/lib:/home/users/mmiletto/install/spack/opt/spack/linux-ubuntu18.04-haswell/gcc-7.4.0/poti-master-hrbowjdpibo5ccqxs2etmducewzdx233/lib:/home/users/mmiletto/install/pajeng-git/lib
MODULEPATH=/home/users/mmiletto/install/spack/share/spack/modules/linux-ubuntu18.04-haswell
LOGNAME=mmiletto
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1038/bus
XDG_RUNTIME_DIR=/run/user/1038
PATH=/home/users/mmiletto/starvz/src:/home/users/mmiletto/starvz/R:/home/users/mmiletto/install/pajeng-git/bin:/home/users/mmiletto/install/spack/opt/spack/linux-ubuntu18.04-haswell/gcc-7.4.0/starpu-1.3.3-2pyv7lz5jjmrphpzxxripicd3ff3xrtt/bin:/home/users/mmiletto/starvz/src:/home/users/mmiletto/starvz/R:/home/users/mmiletto/install/pajeng-git/bin:/home/users/mmiletto/install/spack/opt/spack/linux-ubuntu18.04-haswell/gcc-7.4.0/starpu-develop-mwcgmkfolhjchgurkymy4n2fakrlxpej/bin:/home/users/mmiletto/install/spack/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/users/mmiletto/install/qr_node_flops_exp_ubuntu_1.3.3/bin
SLURM_JOB_ID=341877
LC_IDENTIFICATION=pt_BR.UTF-8
STARPU_NCPU=20
LESSOPEN=| /usr/bin/lesspipe %s
LC_TIME=pt_BR.UTF-8
BASH_FUNC_spack%%=() {  if [ -n "${LD_LIBRARY_PATH-}" ]; then
 export SPACK_LD_LIBRARY_PATH=$LD_LIBRARY_PATH;
 fi;
 if [ -n "${DYLD_LIBRARY_PATH-}" ]; then
 export SPACK_DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH;
 fi;
 if [ -n "${ZSH_VERSION:-}" ]; then
 emulate -L sh;
 fi;
 _sp_flags="";
 while [ ! -z ${1+x} ] && [ "${1#-}" != "${1}" ]; do
 _sp_flags="$_sp_flags $1";
 shift;
 done;
 if [ -n "$_sp_flags" ] && [ "${_sp_flags#*h}" != "${_sp_flags}" ] || [ "${_sp_flags#*V}" != "${_sp_flags}" ]; then
 command spack $_sp_flags "$@";
 return;
 fi;
 _sp_subcommand="";
 if [ ! -z ${1+x} ]; then
 _sp_subcommand="$1";
 shift;
 fi;
 case $_sp_subcommand in 
 "cd")
 _sp_arg="";
 if [ -n "$1" ]; then
 _sp_arg="$1";
 shift;
 fi;
 if [ "$_sp_arg" = "-h" ] || [ "$_sp_arg" = "--help" ]; then
 command spack cd -h;
 else
 LOC="$(spack location $_sp_arg "$@")";
 if [ -d "$LOC" ]; then
 cd "$LOC";
 else
 return 1;
 fi;
 fi;
 return
 ;;
 "env")
 _sp_arg="";
 if [ -n "$1" ]; then
 _sp_arg="$1";
 shift;
 fi;
 if [ "$_sp_arg" = "-h" ] || [ "$_sp_arg" = "--help" ]; then
 command spack env -h;
 else
 case $_sp_arg in 
 activate)
 _a="$@";
 if [ -z ${1+x} ] || [ "${_a#*--sh}" != "$_a" ] || [ "${_a#*--csh}" != "$_a" ] || [ "${_a#*-h}" != "$_a" ]; then
 command spack env activate "$@";
 else
 eval $(command spack $_sp_flags env activate --sh "$@");
 fi
 ;;
 deactivate)
 _a="$@";
 if [ "${_a#*--sh}" != "$_a" ] || [ "${_a#*--csh}" != "$_a" ]; then
 command spack env deactivate "$@";
 else
 if [ -n "$*" ]; then
 command spack env deactivate -h;
 else
 eval $(command spack $_sp_flags env deactivate --sh);
 fi;
 fi
 ;;
 *)
 command spack env $_sp_arg "$@"
 ;;
 esac;
 fi;
 return
 ;;
 "load" | "unload")
 _a=" $@";
 if [ "${_a#* --sh}" != "$_a" ] || [ "${_a#* --csh}" != "$_a" ] || [ "${_a#* -h}" != "$_a" ] || [ "${_a#* --help}" != "$_a" ]; then
 command spack $_sp_flags $_sp_subcommand "$@";
 else
 eval $(command spack $_sp_flags $_sp_subcommand --sh "$@" ||                     echo "return 1");
 fi
 ;;
 *)
 command spack $_sp_flags $_sp_subcommand "$@"
 ;;
 esac
}
_=/usr/bin/env
