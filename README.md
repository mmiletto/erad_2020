# ERAD 2020

Este repositório contém os dados e trechos de código que permitem reproduzir as figuras apresentadas e discutidas no artigo entitulado **Mecanismo de Detecção de Tarefas Anômalas Para a Análise de Desempenho de Aplicações com Tarefas Irregulares**. Este artigo foi submetido na [XX Escola Regional de Alto Desempenho da Região Sul](https://erad2020.inf.ufsm.br/) pelos autores Marcelo Cogo Miletto e Lucas Mello Schnorr.

## Instruções para a reprodução

As instruções para instalação de dependências e criação das figuras podem ser encontradas no arquivo ERAD.org